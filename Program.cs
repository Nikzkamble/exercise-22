﻿using System;

namespace exercise_22
{
    class Program
    {
        static void Main(string[] args)
        {
            var movies = new string[5]  { "Logan", "Deadpool", "Avengers", "Iron-man", "Spider-man"};

            Console.WriteLine(string.Join("\n", movies));
            Console.WriteLine("");
            Console.WriteLine("");
            
            movies[0]="Guardians of galaxy";
            movies[2]="Pirates of the caribbean";
            Console.WriteLine(string.Join("\n",movies));
             Console.WriteLine("");
            Console.WriteLine("");

            Array.Sort(movies);
            Console.WriteLine(string.Join("\n",movies));
              Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("Length of Array:{0,3}", movies.Length);
            Console.WriteLine("");
            Console.WriteLine("");
            var output = String.Join("\n",movies);
            Console.WriteLine(output);
            




        }
    }
}
